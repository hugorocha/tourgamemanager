$(main);
function main()
{
    var username = $("#username_session").text();
    
    if(username == "")
        show();
    else
        hide();
    
    var admin = $("#admin_session").text();
    if(admin == "")
        PF('invitePlayers').disable();
    else
        PF('invitePlayers').enable;            
}

function shButtons()
{
    this.hide();
}

function show()
{
    $("#login").css("display","block");
    $("#signup").css("display","block");
    $("#n_login").show();
    $("#h_login").show();
    $("#n_signup").show();
    $("#li_logout").css("visibility", "hidden");   
    $("#formCreatetourIndex").css("display","none");
    $("#formMytourIndex").css("display","none");
    $("#createclanform").css("display", "none");
}

function hide()
{
    $("#login").css("display","none");
    $("#signup").css("display","none");
    $("#n_login").hide();
    $("#h_login").hide();
    $("#n_signup").hide();
    $("#li_logout").css("visibility", "visible");    
    $("#formCreatetourIndex").css("display","block");
    $("#formMytourIndex").css("display","block");
    $("#createclanform").css("display", "block");
}

function joinTournament()
{
    var username = $("#username_session").text();
    
    if(username == "")
    {
        show();
        PF('joinTournament').disable();
    }
    else
    {
        PF('joinTournament').enable();
        hide();
    } 
    
}

function createClan()
{
    var username = $("#username_session").text();
    
    if(username == "")
    {
        PF('createClan').disable();
        show();
    }
    else
    {
        PF('createClan').enable();
        hide();
    } 
}

function createTour()
{
    var username = $("#username_session").text();
    
    if(username == "")
    {
        PF('createTour').disable();
        show();
    }
    else
    {
        PF('createTour').enable();
        hide();
    } 
}

function Simulation()
{
    var username = $("#tourState").text();
    
    if(username == "Ended")
    {
        PF('tourWinner').disable();
        show();
    }
    else
    {
        PF('tourWinner').enable();
        hide();
    } 
}