package domain;

import domain.Team;
import domain.Tour;
import domain.TourhasTeamPK;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-06-13T10:08:27")
@StaticMetamodel(TourhasTeam.class)
public class TourhasTeam_ { 

    public static volatile SingularAttribute<TourhasTeam, TourhasTeamPK> tourhasTeamPK;
    public static volatile SingularAttribute<TourhasTeam, Boolean> confirmation;
    public static volatile SingularAttribute<TourhasTeam, Team> team;
    public static volatile SingularAttribute<TourhasTeam, Tour> tour;

}