package domain;

import domain.Tour;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-06-13T10:08:27")
@StaticMetamodel(Matches.class)
public class Matches_ { 

    public static volatile SingularAttribute<Matches, Date> dateTime;
    public static volatile SingularAttribute<Matches, Integer> winner;
    public static volatile SingularAttribute<Matches, Integer> teamA;
    public static volatile SingularAttribute<Matches, Integer> idMatch;
    public static volatile SingularAttribute<Matches, Integer> teamB;
    public static volatile CollectionAttribute<Matches, Tour> tourCollection;

}