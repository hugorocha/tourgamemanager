package domain;

import domain.Game;
import domain.Matches;
import domain.Player;
import domain.Team;
import domain.TourHistory;
import domain.TourType;
import domain.TourhasTeam;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-06-13T10:08:27")
@StaticMetamodel(Tour.class)
public class Tour_ { 

    public static volatile SingularAttribute<Tour, Date> dateTime;
    public static volatile SingularAttribute<Tour, Game> gameidGame;
    public static volatile SingularAttribute<Tour, Integer> idTour;
    public static volatile SingularAttribute<Tour, String> description;
    public static volatile CollectionAttribute<Tour, TourhasTeam> tourhasTeamCollection;
    public static volatile CollectionAttribute<Tour, Matches> matchesCollection;
    public static volatile SingularAttribute<Tour, TourType> tourTypeidTourType;
    public static volatile CollectionAttribute<Tour, Player> playerCollection;
    public static volatile CollectionAttribute<Tour, Team> teamCollection;
    public static volatile SingularAttribute<Tour, Float> buyin;
    public static volatile SingularAttribute<Tour, String> name;
    public static volatile SingularAttribute<Tour, Float> prizepool;
    public static volatile CollectionAttribute<Tour, TourHistory> tourHistoryCollection;

}