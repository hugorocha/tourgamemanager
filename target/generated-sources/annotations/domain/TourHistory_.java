package domain;

import domain.Tour;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-06-13T10:08:27")
@StaticMetamodel(TourHistory.class)
public class TourHistory_ { 

    public static volatile SingularAttribute<TourHistory, Tour> touridTour;
    public static volatile SingularAttribute<TourHistory, Integer> pos;
    public static volatile SingularAttribute<TourHistory, Integer> idTourHistory;
    public static volatile SingularAttribute<TourHistory, Integer> team;
    public static volatile SingularAttribute<TourHistory, Float> prize;

}