package domain;

import domain.PlayerhasTeam;
import domain.Tour;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-06-13T10:08:27")
@StaticMetamodel(Player.class)
public class Player_ { 

    public static volatile SingularAttribute<Player, Integer> idPlayer;
    public static volatile SingularAttribute<Player, String> password;
    public static volatile CollectionAttribute<Player, PlayerhasTeam> playerhasTeamCollection;
    public static volatile CollectionAttribute<Player, Tour> tourCollection;
    public static volatile SingularAttribute<Player, String> email;
    public static volatile SingularAttribute<Player, String> username;

}