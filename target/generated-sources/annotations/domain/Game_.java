package domain;

import domain.Tour;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-06-13T10:08:27")
@StaticMetamodel(Game.class)
public class Game_ { 

    public static volatile SingularAttribute<Game, Integer> maxPlayer;
    public static volatile SingularAttribute<Game, Integer> idGame;
    public static volatile SingularAttribute<Game, String> name;
    public static volatile CollectionAttribute<Game, Tour> tourCollection;
    public static volatile SingularAttribute<Game, Integer> minPlayer;
    public static volatile SingularAttribute<Game, String> imgsrc;

}