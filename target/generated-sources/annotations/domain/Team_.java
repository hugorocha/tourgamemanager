package domain;

import domain.PlayerhasTeam;
import domain.Tour;
import domain.TourhasTeam;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-06-13T10:08:27")
@StaticMetamodel(Team.class)
public class Team_ { 

    public static volatile SingularAttribute<Team, String> teamName;
    public static volatile CollectionAttribute<Team, PlayerhasTeam> playerhasTeamCollection;
    public static volatile SingularAttribute<Team, Integer> nElements;
    public static volatile CollectionAttribute<Team, Tour> tourCollection;
    public static volatile CollectionAttribute<Team, TourhasTeam> tourhasTeamCollection;
    public static volatile SingularAttribute<Team, String> imgsrc;
    public static volatile SingularAttribute<Team, Integer> idTeam;

}