package domain;

import domain.Tour;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-06-13T10:08:27")
@StaticMetamodel(TourType.class)
public class TourType_ { 

    public static volatile SingularAttribute<TourType, Integer> maxTeams;
    public static volatile CollectionAttribute<TourType, Tour> tourCollection;
    public static volatile SingularAttribute<TourType, String> rules;
    public static volatile SingularAttribute<TourType, Integer> idTourType;
    public static volatile SingularAttribute<TourType, Integer> nTeams;

}