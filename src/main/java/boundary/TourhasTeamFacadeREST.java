/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import domain.TourhasTeam;
import domain.TourhasTeamPK;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.PathSegment;

/**
 *
 * @author hugo
 */
@Stateless
@Path("tourhasteam")
public class TourhasTeamFacadeREST extends AbstractFacade<TourhasTeam>{

         @PersistenceContext(unitName = "com.mycompany_TqsP1_war_1.0-SNAPSHOTPU")
         private EntityManager em;

         private TourhasTeamPK getPrimaryKey(PathSegment pathSegment) {
                  /*
                   * pathSemgent represents a URI path segment and any associated matrix parameters.
                   * URI path part is supposed to be in form of 'somePath;touridTour=touridTourValue;teamidTeam=teamidTeamValue'.
                   * Here 'somePath' is a result of getPath() method invocation and
                   * it is ignored in the following code.
                   * Matrix parameters are used as field names to build a primary key instance.
                   */
                  domain.TourhasTeamPK key = new domain.TourhasTeamPK();
                  javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
                  java.util.List<String> touridTour = map.get("touridTour");
                  if (touridTour != null && !touridTour.isEmpty()) {
                           key.setTouridTour(new java.lang.Integer(touridTour.get(0)));
                  }
                  java.util.List<String> teamidTeam = map.get("teamidTeam");
                  if (teamidTeam != null && !teamidTeam.isEmpty()) {
                           key.setTeamidTeam(new java.lang.Integer(teamidTeam.get(0)));
                  }
                  return key;
         }

         public TourhasTeamFacadeREST() {
                  super(TourhasTeam.class);
         }

         @POST
         @Override
         @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
         public void create(TourhasTeam entity) {
                  super.create(entity);
         }

         @PUT
         @Path("{id}")
         @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
         public void edit(@PathParam("id") PathSegment id, TourhasTeam entity) {
                  super.edit(entity);
         }

         @DELETE
         @Path("{id}")
         public void remove(@PathParam("id") PathSegment id) {
                  domain.TourhasTeamPK key = getPrimaryKey(id);
                  super.remove(super.find(key));
         }

         @GET
         @Path("{id}")
         @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
         public TourhasTeam find(@PathParam("id") PathSegment id) {
                  domain.TourhasTeamPK key = getPrimaryKey(id);
                  return super.find(key);
         }

         @GET
         @Override
         @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
         public List<TourhasTeam> findAll() {
                  return super.findAll();
         }

         @GET
         @Path("{from}/{to}")
         @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
         public List<TourhasTeam> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
                  return super.findRange(new int[]{from, to});
         }

         @GET
         @Path("count")
         @Produces(MediaType.TEXT_PLAIN)
         public String countREST() {
                  return String.valueOf(super.count());
         }

         @Override
         protected EntityManager getEntityManager() {
                  return em;
         }
         
}
