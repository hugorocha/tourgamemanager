/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boundary;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 *
 * @author hugo
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(boundary.GameFacadeREST.class);
                  resources.add(boundary.MatchesFacadeREST.class);
                  resources.add(boundary.PlayerFacadeREST.class);
                  resources.add(boundary.PlayerhasTeamFacadeREST.class);
                  resources.add(boundary.TeamFacadeREST.class);
                  resources.add(boundary.TourFacadeREST.class);
                  resources.add(boundary.TourHistoryFacadeREST.class);
                  resources.add(boundary.TourTypeFacadeREST.class);
                  resources.add(boundary.TourhasTeamFacadeREST.class);
    }
    
}
