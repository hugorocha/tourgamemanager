/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBean;

import boundary.MatchesFacade;
import boundary.PlayerFacade;
import boundary.PlayerhasTeamFacade;
import boundary.TeamFacade;
import boundary.TourFacade;
import domain.Player;
import domain.Team;
import domain.Matches;
import domain.PlayerhasTeam;
import domain.PlayerhasTeamPK;
import domain.Tour;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.context.FacesContext;

/**
 *
 * @author hugofernandes
 */
@Named(value = "teamView")
@Dependent
public class TeamView {

         @EJB
         private TeamFacade teamFacade;
         @EJB
         private PlayerhasTeamFacade playerhasTeamFacade;
         @EJB
         private MatchesFacade matchFacade;
         @EJB
         private TourFacade tourFacade;
         @EJB
         private PlayerFacade playerFacade;

         private List<String> trophies;
         private static Team team;
         private List<Team> teams;
         private List<String> players;
         private List<String> tours;
         public static String admin = "";
         private static String warning = "";

         /**
          * Creates a new instance of TeamView
          */
         public TeamView() {
                  this.playerFacade = new PlayerFacade();
                  this.tours = new ArrayList();
                  this.trophies = new ArrayList();
                  this.tourFacade = new TourFacade();
                  this.playerhasTeamFacade = new PlayerhasTeamFacade();
                  this.teamFacade = new TeamFacade();
                  this.matchFacade = new MatchesFacade();
                  this.players = new ArrayList();
                  this.teams = new ArrayList();
         }

         public String getWarning() {
                  return this.warning;
         }

         public List<String> getTrophies() {
                  List<Tour> allTours = tourFacade.findAll();

                  for (int i = 0; i < allTours.size(); i++) {
                           Collection<Matches> allMatches = allTours.get(i).getMatchesCollection();

                           Iterator iterator = allMatches.iterator();
                           while (iterator.hasNext()) {
                                    Matches match = (Matches) iterator.next();

                                    if (match.getWinner().equals(team.getIdTeam())) {
                                             trophies.add(allTours.get(i).getName());
                                    }
                           }
                  }

                  if (trophies.size() == 0) {
                           trophies.add("No trophies yet.");
                  }

                  return trophies;
         }

         public List<String> getTours() {
                  List<Matches> AllMatches = matchFacade.findAll();
                  List<Matches> MatchesWon = new ArrayList();

                  for (int i = 0; i < AllMatches.size(); i++) {
                           if (AllMatches.get(i).getWinner().equals(team.getIdTeam())) {
                                    MatchesWon.add(AllMatches.get(i));
                           }
                  }

                  return tours;
         }

         public Team getTeam() {
                  return team;
         }

         public List<String> getPlayers(int teamId) {
                  List<PlayerhasTeam> playerHasTeam = playerhasTeamFacade.findAll();

                  System.out.println("HERE WE GO"+teamId);
                  for (int i = 0; i < playerHasTeam.size(); i++) {
                           if (playerHasTeam.get(i).getTeam().getIdTeam().equals(teamId)) {
                                    players.add(playerHasTeam.get(i).getNickname());
                           }
                  }
                  return players;
         }

         public void addPlayerToTeam(String playerTeams, int playerNum) throws IOException {

                  boolean flag = true, sucess = false;
                  String[] tmp = playerTeams.split(" , ");
                  PlayerhasTeam pHt = new PlayerhasTeam();

                  for (int i = 0; i < tmp.length; i++) {
                           if (tmp[i].equalsIgnoreCase(this.team.getTeamName())) {
                                    flag = false;
                           }
                  }
                  if (!flag) {
                           warning = "Jogador já está na equipa em questão";
                  } else {
                           Player p = getThePlayer(playerNum);
                           if (p != null) {
                                    pHt.setPlayer(p);
                                    pHt.setTeam(this.team);
                                    pHt.setNickname(p.getUsername());
                                    byte[] bool = new byte[1];
                                    bool[0] = (byte) 0;
                                    pHt.setLider(bool);
                                    pHt.setPlayerhasTeamPK(new PlayerhasTeamPK(p.getIdPlayer(), this.team.getIdTeam()));
                                    sucess = true;
                           }
                           if (sucess) {
                                    playerhasTeamFacade.create(pHt);
                                    FacesContext.getCurrentInstance().getExternalContext().redirect("specificTeam.xhtml");
                           }
                  }
         }
         public byte[] stringToBytesASCII(String str) {
                char[] buffer = str.toCharArray();
                byte[] b = new byte[buffer.length];
                for (int i = 0; i < b.length; i++) {
                    b[i] = (byte) buffer[i];
                }
                return b;
            }
         private Player getThePlayer(int num) {
                  List<Player> player = playerFacade.findAll();
                  for (int i = 0; i < player.size(); i++) {
                           if (player.get(i).getIdPlayer() == num) {
                                    return player.get(i);
                           }
                  }
                  return null;
         }

         public void invitePlayers() throws IOException {
                  FacesContext.getCurrentInstance().getExternalContext().redirect("searchPlayer.xhtml");
         }

         public void isLeader() {
                  int idTeam = team.getIdTeam();
                  List<Player> players = playerFacade.findAll();
                  List<PlayerhasTeam> playerHasTeam = playerhasTeamFacade.findAll();
                  boolean entered = false;

                  for (int j = 0; j < playerHasTeam.size(); j++) {
                           System.out.println(playerHasTeam.get(j).getPlayer().getUsername() + " : " + SignupView.username + " : " + playerHasTeam.get(j).getPlayer().getUsername().equals(SignupView.username));

                           if (playerHasTeam.get(j).getPlayer().getUsername().equals(SignupView.username) && playerHasTeam.get(j).getTeam().getIdTeam().equals(idTeam)) {
                                    entered = true;
                                    byte[] buffer = playerHasTeam.get(j).getLider();
                                    String isLeader = "";
                                    for (byte b : buffer) {
                                             isLeader += (char) b;
                                    }

                                    if (isLeader.equals("1")) {
                                             this.admin = playerHasTeam.get(j).getNickname();
                                    } else {
                                             this.admin = "";
                                    }

                                    break;
                           }
                  }

                  if (!entered) {
                           this.admin = "";
                  }
         }

         public void getInfo(Team team) throws IOException {
                  this.team = team;
                  isLeader();
                  FacesContext.getCurrentInstance().getExternalContext().redirect("specificTeam.xhtml#team");

         }

         public String getAdmin() {
                  return admin;
         }

         public List<Team> getTeams() {
                  return teamFacade.findAll();
         }
}
