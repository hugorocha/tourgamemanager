/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ManagedBean;

import boundary.PlayerFacade;
import boundary.PlayerhasTeamFacade;
import boundary.TeamFacade;
import domain.Player;
import domain.Team;
import domain.PlayerhasTeam;
import domain.PlayerhasTeamPK;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.context.FacesContext;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.eclipse.persistence.jpa.jpql.Assert;

/**
 *
 * @author hugofernandes
 */
@Named(value = "createclanView")
@Dependent
public class CreateclanView {

         @EJB
         private TeamFacade teamFacade;
         @EJB
         private PlayerhasTeamFacade playerhasTeamFacade;
         @EJB
         private PlayerFacade playerFacade;

         /**
          * Creates a new instance of createclanView
          */
         public CreateclanView() {
                  teamFacade = new TeamFacade();
         }

         public String ret(String imgSrc) {
                  return imgSrc;
         }

         public void createClan(String teamName, String userName) throws IOException {
                  List<Team> teams = teamFacade.findAll();
                  boolean valido = true, sucess = false;
                  Player p = getThePlayer(userName);
                  Team t = new Team();

                  for (Team team : teams) {
                           if (team.getTeamName().equalsIgnoreCase(teamName)) {
                                    valido = false;
                           }
                  }

                  if (valido && p != null) {
                           try {
                                    t.setTeamName(teamName);
                                    t.setNElements(1);
                                    t.setImgsrc("img/team.jpg");
                                    teamFacade.create(t);

                                    PlayerhasTeam pHt = new PlayerhasTeam();
                                    pHt.setPlayer(p);
                                    pHt.setTeam(t);
                                    pHt.setNickname(p.getUsername());
                                    pHt.setLider(getBytes(false));
                                    pHt.setPlayerhasTeamPK(new PlayerhasTeamPK(p.getIdPlayer(), t.getIdTeam()));
                                    playerhasTeamFacade.create(pHt);
                                    sucess = true;
                           } catch (EJBException e) {
                                    @SuppressWarnings("ThrowableResultIgnored")
                                    Exception cause = e.getCausedByException();
                                    if (cause instanceof ConstraintViolationException) {
                                             @SuppressWarnings("ThrowableResultIgnored")
                                             ConstraintViolationException cve = (ConstraintViolationException) e.getCausedByException();
                                             for (Iterator<ConstraintViolation<?>> it = cve.getConstraintViolations().iterator(); it.hasNext();) {
                                                      ConstraintViolation<? extends Object> v = it.next();
                                                      System.err.println(v);
                                                      System.err.println("==>>" + v.getMessage());
                                             }
                                    }
                                    Assert.fail("ejb exception");
                           }
                  }
                  if (sucess) {
                           FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
                  }

         }

         public static byte[] getBytes(boolean x) {
                  return new byte[]{
                           (byte) (x ? 1 : 0)
                  };
         }

         private Player getThePlayer(String name) {
                  List<Player> player = playerFacade.findAll();
                  for (int i = 0; i < player.size(); i++) {
                           if (player.get(i).getUsername().equalsIgnoreCase(name)) {
                                    return player.get(i);
                           }
                  }
                  return null;
         }

         public void createClanPage() throws IOException {
                  FacesContext.getCurrentInstance().getExternalContext().redirect("createClan.xhtml#clan");
         }
}
