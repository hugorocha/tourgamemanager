/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hugofernandes
 */
@Entity
@Table(name = "Player_has_Team")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PlayerhasTeam.findAll", query = "SELECT p FROM PlayerhasTeam p"),
    @NamedQuery(name = "PlayerhasTeam.findByPlayeridPlayer", query = "SELECT p FROM PlayerhasTeam p WHERE p.playerhasTeamPK.playeridPlayer = :playeridPlayer"),
    @NamedQuery(name = "PlayerhasTeam.findByTeamidTeam", query = "SELECT p FROM PlayerhasTeam p WHERE p.playerhasTeamPK.teamidTeam = :teamidTeam"),
    @NamedQuery(name = "PlayerhasTeam.findByNickname", query = "SELECT p FROM PlayerhasTeam p WHERE p.nickname = :nickname")})
public class PlayerhasTeam implements Serializable {

         @Lob
         @Column(name = "lider")
         private byte[] lider;
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PlayerhasTeamPK playerhasTeamPK;
    @Size(max = 45)
    @Column(name = "nickname")
    private String nickname;
    @JoinColumn(name = "Player_idPlayer", referencedColumnName = "idPlayer", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Player player;
    @JoinColumn(name = "Team_idTeam", referencedColumnName = "idTeam", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Team team;

    public PlayerhasTeam() {
    }

    public PlayerhasTeam(PlayerhasTeamPK playerhasTeamPK) {
        this.playerhasTeamPK = playerhasTeamPK;
    }

    public PlayerhasTeam(int playeridPlayer, int teamidTeam) {
        this.playerhasTeamPK = new PlayerhasTeamPK(playeridPlayer, teamidTeam);
    }

    public PlayerhasTeamPK getPlayerhasTeamPK() {
        return playerhasTeamPK;
    }

    public void setPlayerhasTeamPK(PlayerhasTeamPK playerhasTeamPK) {
        this.playerhasTeamPK = playerhasTeamPK;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }


    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (playerhasTeamPK != null ? playerhasTeamPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PlayerhasTeam)) {
            return false;
        }
        PlayerhasTeam other = (PlayerhasTeam) object;
        if ((this.playerhasTeamPK == null && other.playerhasTeamPK != null) || (this.playerhasTeamPK != null && !this.playerhasTeamPK.equals(other.playerhasTeamPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domain.PlayerhasTeam[ playerhasTeamPK=" + playerhasTeamPK + " ]";
    }

         public byte[] getLider() {
                  return lider;
         }

         public void setLider(byte[] lider) {
                  this.lider = lider;
         }
    
}
