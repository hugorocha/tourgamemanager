/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hugofernandes
 */
@Entity
@Table(name = "Tour_has_Team")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TourhasTeam.findAll", query = "SELECT t FROM TourhasTeam t"),
    @NamedQuery(name = "TourhasTeam.findByTouridTour", query = "SELECT t FROM TourhasTeam t WHERE t.tourhasTeamPK.touridTour = :touridTour"),
    @NamedQuery(name = "TourhasTeam.findByTeamidTeam", query = "SELECT t FROM TourhasTeam t WHERE t.tourhasTeamPK.teamidTeam = :teamidTeam"),
    @NamedQuery(name = "TourhasTeam.findByConfirmation", query = "SELECT t FROM TourhasTeam t WHERE t.confirmation = :confirmation")})
public class TourhasTeam implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TourhasTeamPK tourhasTeamPK;
    @Column(name = "confirmation")
    private Boolean confirmation;
    @JoinColumn(name = "Tour_idTour", referencedColumnName = "idTour", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Tour tour;
    @JoinColumn(name = "Team_idTeam", referencedColumnName = "idTeam", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Team team;

    public TourhasTeam() {
    }

    public TourhasTeam(TourhasTeamPK tourhasTeamPK) {
        this.tourhasTeamPK = tourhasTeamPK;
    }

    public TourhasTeam(int touridTour, int teamidTeam) {
        this.tourhasTeamPK = new TourhasTeamPK(touridTour, teamidTeam);
    }

    public TourhasTeamPK getTourhasTeamPK() {
        return tourhasTeamPK;
    }

    public void setTourhasTeamPK(TourhasTeamPK tourhasTeamPK) {
        this.tourhasTeamPK = tourhasTeamPK;
    }

    public Boolean getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(Boolean confirmation) {
        this.confirmation = confirmation;
    }

    public Tour getTour() {
        return tour;
    }

    public void setTour(Tour tour) {
        this.tour = tour;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tourhasTeamPK != null ? tourhasTeamPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TourhasTeam)) {
            return false;
        }
        TourhasTeam other = (TourhasTeam) object;
        if ((this.tourhasTeamPK == null && other.tourhasTeamPK != null) || (this.tourhasTeamPK != null && !this.tourhasTeamPK.equals(other.tourhasTeamPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domain.TourhasTeam[ tourhasTeamPK=" + tourhasTeamPK + " ]";
    }
    
}
