/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author hugofernandes
 */
@Entity
@Table(name = "TourType")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TourType.findAll", query = "SELECT t FROM TourType t"),
    @NamedQuery(name = "TourType.findByIdTourType", query = "SELECT t FROM TourType t WHERE t.idTourType = :idTourType"),
    @NamedQuery(name = "TourType.findByRules", query = "SELECT t FROM TourType t WHERE t.rules = :rules"),
    @NamedQuery(name = "TourType.findByNTeams", query = "SELECT t FROM TourType t WHERE t.nTeams = :nTeams"),
    @NamedQuery(name = "TourType.findByMaxTeams", query = "SELECT t FROM TourType t WHERE t.maxTeams = :maxTeams")})
public class TourType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTourType")
    private Integer idTourType;
    @Size(max = 200)
    @Column(name = "rules")
    private String rules;
    @Column(name = "nTeams")
    private Integer nTeams;
    @Basic(optional = false)
    @NotNull
    @Column(name = "maxTeams")
    private int maxTeams;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tourTypeidTourType")
    private Collection<Tour> tourCollection;

    public TourType() {
    }

    public TourType(Integer idTourType) {
        this.idTourType = idTourType;
    }

    public TourType(Integer idTourType, int maxTeams) {
        this.idTourType = idTourType;
        this.maxTeams = maxTeams;
    }

    public Integer getIdTourType() {
        return idTourType;
    }

    public void setIdTourType(Integer idTourType) {
        this.idTourType = idTourType;
    }

    public String getRules() {
        return rules;
    }

    public void setRules(String rules) {
        this.rules = rules;
    }

    public Integer getNTeams() {
        return nTeams;
    }

    public void setNTeams(Integer nTeams) {
        this.nTeams = nTeams;
    }

    public int getMaxTeams() {
        return maxTeams;
    }

    public void setMaxTeams(int maxTeams) {
        this.maxTeams = maxTeams;
    }

    @XmlTransient
    public Collection<Tour> getTourCollection() {
        return tourCollection;
    }

    public void setTourCollection(Collection<Tour> tourCollection) {
        this.tourCollection = tourCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTourType != null ? idTourType.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TourType)) {
            return false;
        }
        TourType other = (TourType) object;
        if ((this.idTourType == null && other.idTourType != null) || (this.idTourType != null && !this.idTourType.equals(other.idTourType))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domain.TourType[ idTourType=" + idTourType + " ]";
    }
    
}
