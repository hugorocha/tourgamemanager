/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import domain.PlayerhasTeam;
import domain.Tour;
import domain.TourhasTeam;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author hugo
 */
@Entity
@Table(name = "Team")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Team.findAll", query = "SELECT t FROM Team t"),
    @NamedQuery(name = "Team.findByIdTeam", query = "SELECT t FROM Team t WHERE t.idTeam = :idTeam"),
    @NamedQuery(name = "Team.findByTeamName", query = "SELECT t FROM Team t WHERE t.teamName = :teamName"),
    @NamedQuery(name = "Team.findByNElements", query = "SELECT t FROM Team t WHERE t.nElements = :nElements")})
public class Team implements Serializable {
    @Size(max = 100)
    @Column(name = "imgsrc")
    private String imgsrc;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "team")
    private Collection<TourhasTeam> tourhasTeamCollection;

    @JoinTable(name = "Tour_has_Team", joinColumns = {
        @JoinColumn(name = "Team_idTeam", referencedColumnName = "idTeam")}, inverseJoinColumns = {
        @JoinColumn(name = "Tour_idTour", referencedColumnName = "idTour")})
    @ManyToMany
    private Collection<Tour> tourCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "team")
    private Collection<PlayerhasTeam> playerhasTeamCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTeam")
    private Integer idTeam;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "teamName")
    private String teamName;
    @Column(name = "nElements")
    private Integer nElements;

    public Team() {
    }

    public Team(Integer idTeam) {
        this.idTeam = idTeam;
    }

    public Team(Integer idTeam, String teamName) {
        this.idTeam = idTeam;
        this.teamName = teamName;
    }

    public Integer getIdTeam() {
        return idTeam;
    }

    public void setIdTeam(Integer idTeam) {
        this.idTeam = idTeam;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public Integer getNElements() {
        return nElements;
    }

    public void setNElements(Integer nElements) {
        this.nElements = nElements;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTeam != null ? idTeam.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Team)) {
            return false;
        }
        Team other = (Team) object;
        if ((this.idTeam == null && other.idTeam != null) || (this.idTeam != null && !this.idTeam.equals(other.idTeam))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.tqsp1.Team[ idTeam=" + idTeam + " ]";
    }

    @XmlTransient
    public Collection<Tour> getTourCollection() {
        return tourCollection;
    }

    public void setTourCollection(Collection<Tour> tourCollection) {
        this.tourCollection = tourCollection;
    }

    @XmlTransient
    public Collection<PlayerhasTeam> getPlayerhasTeamCollection() {
        return playerhasTeamCollection;
    }

    public void setPlayerhasTeamCollection(Collection<PlayerhasTeam> playerhasTeamCollection) {
        this.playerhasTeamCollection = playerhasTeamCollection;
    }

    public String getImgsrc() {
        return imgsrc;
    }

    public void setImgsrc(String imgsrc) {
        this.imgsrc = imgsrc;
    }

    @XmlTransient
    public Collection<TourhasTeam> getTourhasTeamCollection() {
        return tourhasTeamCollection;
    }

    public void setTourhasTeamCollection(Collection<TourhasTeam> tourhasTeamCollection) {
        this.tourhasTeamCollection = tourhasTeamCollection;
    }
    
}
