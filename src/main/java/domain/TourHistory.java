/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hugofernandes
 */
@Entity
@Table(name = "TourHistory")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TourHistory.findAll", query = "SELECT t FROM TourHistory t"),
    @NamedQuery(name = "TourHistory.findByIdTourHistory", query = "SELECT t FROM TourHistory t WHERE t.idTourHistory = :idTourHistory"),
    @NamedQuery(name = "TourHistory.findByTeam", query = "SELECT t FROM TourHistory t WHERE t.team = :team"),
    @NamedQuery(name = "TourHistory.findByPos", query = "SELECT t FROM TourHistory t WHERE t.pos = :pos"),
    @NamedQuery(name = "TourHistory.findByPrize", query = "SELECT t FROM TourHistory t WHERE t.prize = :prize")})
public class TourHistory implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idTourHistory")
    private Integer idTourHistory;
    @Column(name = "team")
    private Integer team;
    @Column(name = "pos")
    private Integer pos;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "prize")
    private Float prize;
    @JoinColumn(name = "Tour_idTour", referencedColumnName = "idTour")
    @ManyToOne(optional = false)
    private Tour touridTour;

    public TourHistory() {
    }

    public TourHistory(Integer idTourHistory) {
        this.idTourHistory = idTourHistory;
    }

    public Integer getIdTourHistory() {
        return idTourHistory;
    }

    public void setIdTourHistory(Integer idTourHistory) {
        this.idTourHistory = idTourHistory;
    }

    public Integer getTeam() {
        return team;
    }

    public void setTeam(Integer team) {
        this.team = team;
    }

    public Integer getPos() {
        return pos;
    }

    public void setPos(Integer pos) {
        this.pos = pos;
    }

    public Float getPrize() {
        return prize;
    }

    public void setPrize(Float prize) {
        this.prize = prize;
    }

    public Tour getTouridTour() {
        return touridTour;
    }

    public void setTouridTour(Tour touridTour) {
        this.touridTour = touridTour;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTourHistory != null ? idTourHistory.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TourHistory)) {
            return false;
        }
        TourHistory other = (TourHistory) object;
        if ((this.idTourHistory == null && other.idTourHistory != null) || (this.idTourHistory != null && !this.idTourHistory.equals(other.idTourHistory))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "domain.TourHistory[ idTourHistory=" + idTourHistory + " ]";
    }
    
}
