/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JUnitTest;

import ManagedBean.SignupView;
import domain.Player;
import domain.PlayerhasTeam;
import java.util.ArrayList;
import java.util.List;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author acardoso
 */
public class SignupViewTest {
    
    private Player p1, p2;
    private List<Player> lPlayers;
    
    public SignupViewTest() {
    }
    
    @Before
    public void setUp() {
        
        p1 = new Player(0, "user1", "userpass", "user@asd.com");
        p2 = new Player(1, "user2", "userpass", "user@asd.com");
        lPlayers = new ArrayList<>();
        lPlayers.add( p1 );
        lPlayers.add( p2 );
    }
    
    /**
     * Test of getUsername method, of class SignupView.
     */
    @Test
    public void signup() {
        System.out.println("getUsername");
        
        String username="", password="", email="", warning_signup;
        
        boolean valido = true;

        if (!username.equals("") && !password.equals("") && !email.equals("")) {
            List<Player> players = lPlayers;//playerFacade.findAll();

            for (Player player : players) {
                if (player.getUsername().equals(username)) {
                    valido = false;
                }
                warning_signup = "The username you're trying to sign up already exists. Please, try another one!";
            }

        } else {
            valido = false;
            warning_signup = "The fields shouldn't be empty!";
        }
        
        assertFalse( valido );
        username = "user";
        password = "pass";
        email = "user@asd.pt";
        valido = true;
        
        if (valido) {
            warning_signup = "";
            Player p = new Player();
            p.setUsername(username);
            p.setPassword(password);
            p.setEmail(email);
            //playerFacade.create(p);
            //this.username = username;
        }
    }
    
    @Test
    public void login() {
        String username = "user2", password = "userpass";
        boolean valido = false;

        for (Player p : lPlayers ) {
            if (p.getUsername().equals(username) && p.getPassword().equals(password)) {
                valido = true;
            }
        }

        assertTrue(valido);
    }

}
