/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package REST;

import javax.enterprise.context.Dependent;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.primefaces.json.JSONArray;

/**
 *
 * @author hugofernandes
 */
public class TeamTest {
    private Client client;
    private WebTarget target;
   
    public TeamTest() {
    }
    
    @Before
    public void setUp() {
        this.client = ClientBuilder.newBuilder().build();
        this.target = (WebTarget) client.target("http://deti-tqs-vm4.ua.pt:8080/TqsP1v2/webresources/team");               
    }
    
    @Test
    public void testTeamAvailable()
    {
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));
        
        String allTeams = response.readEntity(String.class);
    
        JSONArray teamJSONarray = new JSONArray(allTeams);
        
        boolean teamNameAvailable = false;
        
        for(int i = 0; i < teamJSONarray.length(); i++)
        {
            
            if(teamJSONarray.getJSONObject(i).getString("teamName").equals("Team 1"))
                teamNameAvailable = true;
        }
        
        assertEquals("Team 1 doesn't exist on REST, but it should!", teamNameAvailable, true);                    
    }
    
    @Test
    public void testTeamNotAvailable()
    {
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));
        
        String allTeams = response.readEntity(String.class);
    
        JSONArray teamJSONarray = new JSONArray(allTeams);
        
        boolean teamNameAvailable = false;
        
        for(int i = 0; i < teamJSONarray.length(); i++)
        {
            
            if(teamJSONarray.getJSONObject(i).getString("teamName").equals("Team77"))
                teamNameAvailable = true;
        }
        
        assertEquals("Team77 exists on REST, but it shouldn't!", teamNameAvailable, false);
    }
            
    @After
    public void tearDown() {
    }
    
}
