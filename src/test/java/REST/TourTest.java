package REST;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.primefaces.json.JSONArray;

/**
 *
 * @author hugofernandes
 */
public class TourTest {
    private Client client;
    private WebTarget target;
    
    public TourTest() {
        
    }
    
    @Before
    public void setUp() {
        this.client = ClientBuilder.newBuilder().build();
        this.target = (WebTarget) client.target("http://deti-tqs-vm4.ua.pt:8080/TqsP1v2/webresources/tour");               
    }
    
    @Test
    public void testTourAvailable()
    {
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));
        
        String allTours = response.readEntity(String.class);
    
        JSONArray tourJSONarray = new JSONArray(allTours);
        boolean tourNameAvailable = false;
        
        System.out.println(tourJSONarray);
        for(int i = 0; i < tourJSONarray.length(); i++)
        {
            
            if(tourJSONarray.getJSONObject(i).getString("name").equals("Torneio de CS"))
                tourNameAvailable = true;
        }
        
        assertEquals("Torneio de CS doesn't exist on REST, but it should!", tourNameAvailable, true);            
    }

    @Test
    public void testTourNotAvailable()
    {
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));
        
        String allTours = response.readEntity(String.class);
    
        JSONArray tourJSONarray = new JSONArray(allTours);
        boolean tourNameAvailable = false;
        
        System.out.println(tourJSONarray);
        for(int i = 0; i < tourJSONarray.length(); i++)
        {
            
            if(tourJSONarray.getJSONObject(i).getString("name").equals("Torneio de sueca"))
                tourNameAvailable = true;
        }
        
        assertEquals("Torneio de sueca exists on REST, but it shouldn't!", tourNameAvailable, false);            
    }
    
    @After
    public void tearDown() {
    }
    
}
