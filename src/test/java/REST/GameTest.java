package REST;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.primefaces.json.JSONArray;

/**
 *
 * @author hugofernandes
 */
public class GameTest {
    private Client client;
    private WebTarget target;
    
    public GameTest() {
    }
    
    @Before
    public void setUp() {
        this.client = ClientBuilder.newBuilder().build();
        this.target = (WebTarget) client.target("http://deti-tqs-vm4.ua.pt:8080/TqsP1v2/webresources/game");               
    }
    
    @Test
    public void testGameAvailable()
    {        
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));
        
        String allGames = response.readEntity(String.class);
    
        JSONArray gameJSONarray = new JSONArray(allGames);
        boolean gameNameAvailable = false;
        
        for(int i = 0; i < gameJSONarray.length(); i++)
        {
            
            if(gameJSONarray.getJSONObject(i).getString("name").equals("LOL"))
                gameNameAvailable = true;
        }
        
        assertEquals("LOL doesn't exist on REST, but it should!", gameNameAvailable, true);    
    }
    
    @Test
    public void testGameNotAvailable()
    {
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));
        
        String allGames = response.readEntity(String.class);
    
        JSONArray gameJSONarray = new JSONArray(allGames);
        boolean gameNameAvailable = false;

        System.out.println(gameJSONarray);
        for(int i = 0; i < gameJSONarray.length(); i++)
        {
            
            if(gameJSONarray.getJSONObject(i).getString("name") == "doesnotexist")
                gameNameAvailable = true;
        }
        
        assertEquals("doesnotexist exists on REST, but it shouldn't!", gameNameAvailable, false);    
        
    }
    
    @After
    public void tearDown() {
    }
    
}
