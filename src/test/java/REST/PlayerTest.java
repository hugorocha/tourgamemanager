/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package REST;

import javax.enterprise.context.Dependent;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.primefaces.json.JSONArray;

/**
 *
 * @author hugofernandes
 */
public class PlayerTest {
    private Client client;
    private WebTarget target;
    
    public PlayerTest() {
    }
    
    @Before
    public void setUp() {
        this.client = ClientBuilder.newBuilder().build();
        this.target = (WebTarget) client.target("http://deti-tqs-vm4.ua.pt:8080/TqsP1v2/webresources/player");               
    }
    
    @Test
    public void testPlayerAvailable()
    {
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));
        
        String allPlayers = response.readEntity(String.class);
    
        JSONArray playerJSONarray = new JSONArray(allPlayers);
        
        boolean playerUsernameAvailable = false;
        
        for(int i = 0; i < playerJSONarray.length(); i++)
        {
            
            if(playerJSONarray.getJSONObject(i).getString("username").equals("user1"))
                playerUsernameAvailable = true;
        }
        
        assertEquals("user1 doesn't exist on REST, but it should!", playerUsernameAvailable, true);            
    }
    
    @Test
    public void testPlayerNotAvailable()
    {
        Response response = target.request(MediaType.APPLICATION_JSON).get();
        assertThat(response.getStatus(), CoreMatchers.is(200));
        
        String allPlayers = response.readEntity(String.class);
    
        JSONArray playerJSONarray = new JSONArray(allPlayers);
        
        boolean playerUsernameAvailable = false;
        
        for(int i = 0; i < playerJSONarray.length(); i++)
        {
            
            if(playerJSONarray.getJSONObject(i).getString("username").equals("thisuserdoesnotexist"))
                playerUsernameAvailable = true;
        }
        
        assertEquals("thisuserdoesnotexist exists on REST, but it shouldn't!", playerUsernameAvailable, false);    

    }
    
    @After
    public void tearDown() {
    }
    
}
