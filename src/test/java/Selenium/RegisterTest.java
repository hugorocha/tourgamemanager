package Selenium;


import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class RegisterTest {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    System.setProperty("webdriver.chrome.driver", "chromedriver");

    driver = new ChromeDriver();
    baseUrl = "http://deti-tqs-vm4.ua.pt:8080";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  //@Test
  public void testRegister() throws Exception {
    driver.get(baseUrl + "/TqsP1v2/");
    
    if(driver.findElement(By.id("logoutform:n_logout")).isDisplayed()){
        driver.findElement(By.id("logoutform:n_logout")).click();
    }
    
    driver.findElement(By.id("n_signup")).click();
    driver.findElement(By.id("regform:user")).clear();
    driver.findElement(By.id("regform:user")).sendKeys("testeUser");
    driver.findElement(By.id("regform:pass")).clear();
    driver.findElement(By.id("regform:pass")).sendKeys("123");
    driver.findElement(By.id("regform:morada")).clear();
    driver.findElement(By.id("regform:morada")).sendKeys("teste@teste.pt");
    Thread.sleep(700);

  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
