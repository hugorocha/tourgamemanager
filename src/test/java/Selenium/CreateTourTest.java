package Selenium;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateTourTest {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @Before
  public void setUp() throws Exception {
    System.setProperty("webdriver.chrome.driver", "chromedriver");

    driver = new ChromeDriver();
    baseUrl = "http://deti-tqs-vm4.ua.pt:8080";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  //@Test
  public void testCreateTour() throws Exception {
    driver.get(baseUrl + "/TqsP1v2/");
    if(driver.findElement(By.id("logoutform:n_logout")).isDisplayed()){
        driver.findElement(By.id("logoutform:n_logout")).click();
    }
    driver.findElement(By.id("login")).click();
    driver.findElement(By.id("loginform:username")).clear();
    driver.findElement(By.id("loginform:username")).sendKeys("bruno");
    driver.findElement(By.id("loginform:password")).clear();
    driver.findElement(By.id("loginform:password")).sendKeys("123");
    driver.findElement(By.id("loginform:logBtn")).click();

    driver.get(baseUrl + "/TqsP1v2/faces/createTour.xhtml#tour");

    driver.findElement(By.id("createtourform:tourName")).clear();
    driver.findElement(By.id("createtourform:tourName")).sendKeys("tour1");
    driver.findElement(By.id("createtourform:description")).clear();
    driver.findElement(By.id("createtourform:description")).sendKeys("tour1");
    driver.findElement(By.id("createtourform:game")).clear();
    driver.findElement(By.id("createtourform:game")).sendKeys("Call of Duty 4");
    driver.findElement(By.id("createtourform:dateTime")).clear();
    driver.findElement(By.id("createtourform:dateTime")).sendKeys("12/04/2016");
    driver.findElement(By.id("createtourform:price")).clear();
    driver.findElement(By.id("createtourform:price")).sendKeys("15");
    driver.findElement(By.id("createtourform:tourStyle")).clear();
    driver.findElement(By.id("createtourform:tourStyle")).sendKeys("5x5");
  }

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
